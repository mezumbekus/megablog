<?php

namespace app\assets;

use yii\web\AssetBundle;

class BlogAsset extends AssetBundle
{
    public $baseUrl = '@web/blog';
    public $basePath = '@webroot';

    public $css = [
        "css/bootstrap.min.css",
        "css/font-awesome.min.css",
        "css/animate.min.css",
        "css/owl.carousel.css",
        "css/owl.theme.css",
        "css/owl.transitions.css",
        "css/style.css",
        "css/responsive.css",
    ];

    public $js = [
        "js/jquery-1.11.3.min.js",
        "js/bootstrap.min.js",
        "js/owl.carousel.min.js",
        "js/jquery.stickit.min.js",
        "js/menu.js",
        "js/scripts.js",
    ];

}