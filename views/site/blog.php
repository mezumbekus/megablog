<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <article class="post">
                    <div class="post-thumb">
                        <a href="blog.html"><img src="<?= $article->getIMage(); ?>" alt=""></a>
                    </div>
                    <div class="post-content">
                        <header class="entry-header text-center text-uppercase">
                            <h6><a href="#"> Travel</a></h6>

                            <h1 class="entry-title"><a href="blog.html"><?= $article->title; ?></a></h1>


                        </header>
                        <div class="entry-content">

                           <?= $article->content?>
                        </div>



                    </div>
                </article>
                <h4>3 comments</h4>
                <?php foreach ($comments as $comment): ?>
                <div class="bottom-comment">


                    <div class="comment-img">
                        <img class="img-circle" src="" alt="">
                    </div>

                    <div class="comment-text">
                        <a href="#" class="replay btn pull-right"> Ответить</a>
                        <h5><?= $comment->user->name; ?></h5>

                        <p class="comment-date">
                            <?= $comment->getDate(); ?>
                        </p>


                        <p class="para"><?= $comment->text; ?></p>
                    </div>
                </div>
                <?php endforeach; ?>
                <!-- end bottom comment-->


                <div class="leave-comment"><!--leave comment-->
                    <?php if(!Yii::$app->user->isGuest): ?>
                    <h4>Оставь коммент</h4>


                    <?php ActiveForm::begin(['action' => ['site/comment','id' => $article->id],'options' => ['class' => 'form-horizontal contact-form','role' => 'form']]) ?>

                        <div class="form-group">
                            <div class="col-md-12">
										<textarea class="form-control" rows="6" name="text"
                                                  placeholder="Write Massage"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn send-btn">Оставить коммент</button>
                    <?php ActiveForm::end() ?>

                <?php else: ?>
                    <h4>Залогиньтесь, чобы оставлять комментарии</h4>
                <?php endif; ?>
                </div>
            </div>

            <?= $this->render('/sidebar/sidebar',[
                'article' => $article,
                'popular' => $popular,
                'recent' => $recent,
                'categories' => $categories,
            ]) ?>
            </div>
        </div>
    </div>
</div>