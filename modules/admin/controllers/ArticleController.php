<?php

namespace app\modules\admin\controllers;

use app\models\Category;
use app\models\Tag;
use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ImageUpload;
use yii\web\UploadedFile;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Загружает картинку
     * @var integer $id айдишник статьи
     *
     */
    public function actionSetImage($id)
    {
        $model = new ImageUpload();
        $article = $this->findModel($id);
        if(Yii::$app->request->isPost){
            $file = UploadedFile::getInstance($model,'image');
            if($article->saveImage($model->uploadFile($file,$article->image))){
                return $this->redirect(['view','id'=>$id]);
            }

        }
        return $this->render('image',[
            'model' => $model,
        ]);
    }
    /**
     * Установка категории
     * @var integer $id айдишник статьи
     */
    public function actionSetCategory($id)
    {
        $model = $this->findModel($id);
        $categoriesList = ArrayHelper::map(Category::find()->all(),'id','title');
        $selectedCategory = ($model->category) ? $model->category->id : 0;
        if(Yii::$app->request->isPost) {
            $category = Yii::$app->request->post('category');
            if($model->saveCategory($category))
                return $this->redirect(['view','id'=>$id]);
        }
        return $this->render('category',
            [
                'article' => $model,
                'selectedCategory' => $selectedCategory,
                'categoryList' => $categoriesList,
            ]);
    }

    /**
     * Установка тега
     */
    public function actionSetTags($id)
    {
        $article = $this->findModel($id);
        $selectedtags = $article->getSelectedTags();
        $alltags = ArrayHelper::map(Tag::find()->all(),'id','title');
        if(Yii::$app->request->isPost){
            $tags = Yii::$app->request->post('tags');
            $article->saveTags($tags);
            $this->redirect(['view','id'=>$id]);
        }
        return $this->render('tags',[
            'selectedtags' => $selectedtags,
            'alltags' => $alltags,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        $categories = ArrayHelper::map(Category::find()->all(),'id','title');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'categories' => $categories,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $categories = ArrayHelper::map(Category::find()->all(),'id','title');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'categories' => $categories,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
