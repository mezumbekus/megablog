<?php

namespace app\controllers;

use app\models\Article;
use app\models\Category;
use app\models\Comment;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\Pagination;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $query = Article::find();
        $count = $query->count();
        $pagination = new Pagination(['totalCount'=> $count,'pageSize'=> 1]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $popular = Article::find()->orderBy('viewed desc')->limit(3)->all();
        $recent = Article::find()->orderBy('date asc')->limit(5)->all();
        $categories = Category::find()->all();
        return $this->render('index',[
            'articles' => $articles,
            'pagination' => $pagination,
            'popular' => $popular,
            'recent' => $recent,
            'categories' => $categories,
        ]);
    }


    /**
     * @param $id Article ID
     */
    public function actionComment($id)
    {
        $comment = new Comment();
        if(Yii::$app->request->isPost){
            $comment->text = Yii::$app->request->post('text');
            $comment->date = Date('Y-m-d');
            $comment->user_id = Yii::$app->user->id;
            $comment->article_id = $id;
            if($comment->saveComment())
                return $this->goBack();
        }

    }

    public function actionBlog($id)
    {
        $article = Article::findOne(['id'=> $id]);
        $popular = Article::find()->orderBy('viewed desc')->limit(3)->all();
        $recent = Article::find()->orderBy('date asc')->limit(5)->all();
        $categories = Category::find()->all();
        $comments = $article->comments;

        return $this->render('blog',[
            'article' => $article,
            'popular' => $popular,
            'recent' => $recent,
            'categories' => $categories,
            'comments' => $comments,
        ]);
    }

    public function actionCategory($id)
    {
        $count = Article::find()->where(['category_id' => $id])->count();
        $pagination = new Pagination(['totalCount'=> $count,'pageSize'=> 1]);
        $articles = Article::find()->where(['category_id' => $id])
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $popular = Article::find()->orderBy('viewed desc')->limit(3)->all();
        $recent = Article::find()->orderBy('date asc')->limit(5)->all();
        $categories = Category::find()->all();

        return $this->render('category',[
            'articles' => $articles,
            'pagination' => $pagination,
            'popular' => $popular,
            'recent' => $recent,
            'categories' => $categories,
        ]);
    }

    public function geyComments($id)
    {
        $comments = Comment::findAll(['article_id' => $id]);
        return $comments;
    }
}
