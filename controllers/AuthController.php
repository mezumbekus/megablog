<?php

namespace app\controllers;

use app\models\LoginForm;
use yii\web\Controller;
use app\models\User;
use Yii;
use app\models\SignUp;


class AuthController extends Controller
{
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionRegister()
    {
        $model = new SignUp();
        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            return $this->redirect(['/auth/login']);
        }
        return $this->render('register',[
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


}