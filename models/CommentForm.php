<?php

namespace app\models;

use yii\base\Model;

class CommentForm extends Model
{
    public $text;

    public function rules()
    {
        return [
            [['comment'],'required'],
            [['comment'],'string'],
        ];
    }

}