<?php

namespace app\models;
use yii\base\Model;

class SignUp extends Model
{
    public $name;
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['name','email','password'],'required'],
            [['name'],'string'],
            [['email'],'email'],
            [['email'],'unique','targetClass' => 'app\models\User','targetAttribute' => 'email'],
        ];
    }

    public function register()
    {
        if($this->validate()){
            $user = new User();
            $user->email = $this->email;
            $user->password = md5($this->password);
            $user->name = $this->name;
            return $user->save(false);
        }
    }
}
