<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class ImageUpload extends Model
{
    public $image;
    const MAX_LENGHT_FILE = 12;

    public function rules()
    {
        return [
          [['image'],'file','extensions' => ['jpg','png']],
        ];
    }

    public function uploadFile(UploadedFile $file, $currentImage)
    {
        $this->image = $file;
        if($this->validate()) {
            $this->deleteCurrentImage($currentImage);
            $newname = $this->generateFileName($file);
            $file->saveAs($this->getFolder() . $newname);
            return $newname;
        }
    }

    public function getFolder()
    {
        return Yii::getAlias('@webroot') . '/uploads/';
    }

    public function generateFileName($file)
    {
        return substr(md5(uniqid($file->baseName)), 0, self::MAX_LENGHT_FILE) . '.' . $file->extension;
    }

    public function deleteCurrentImage($currentImage)
    {
        if ($currentImage && is_file($this->getFolder().$currentImage)) {
            unlink($this->getFolder() . $currentImage);
        }
    }


}