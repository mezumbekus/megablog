<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $date
 * @property string $image
 * @property int $viewed
 * @property int $user_id
 * @property int $status
 * @property int $category_id
 *
 * @property ArticleTag[] $articleTags
 * @property Comment[] $comments
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'],'required'],
            [['title','description','content'],'string'],
            [['date'],'date','format' => 'php:Y-m-d'],
            [['date'],'default', 'value' => date('Y-m-d')],
            [['title'], 'string','max' => 255],
            [['category_id'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'content' => 'Контент',
            'date' => 'Date',
            'image' => 'Image',
            'viewed' => 'Viewed',
            'user_id' => 'User ID',
            'status' => 'Status',
            'category_id' => 'Категория',
        ];
    }

  public function saveImage($filename)
  {
      $this->image = $filename;
      return $this->save(false);
  }

  public function deleteImage()
  {
      $model = new ImageUpload();
      $model->deleteCurrentImage($this->image);
  }

  public function beforeDelete()
  {
      $this->deleteImage();
      return parent::beforeDelete();
  }

  public function getImage()
  {
      $path = Yii::getAlias('@web').'/uploads/';
      return ($this->image) ? $path.$this->image : $path.'no-image.png';
  }

  public function getCategory()
  {
      return $this->hasOne(Category::className(),['id' => 'category_id']);
  }

  public function saveCategory($id)
  {
      $this->category_id = $id;
      return $this->save();
  }

  public function getTags()
  {
      return $this->hasMany(Tag::className(),['id' => 'tag_id'])->viaTable('article_tag',['article_id' => 'id']);
  }

  public function getSelectedTags()
  {
      return ArrayHelper::getColumn($this->getTags()->select('id')->asArray()->all(),'id');

  }

  public function saveTags($tags)
  {
      ArticleTag::deleteAll(['article_id' => $this->id]);
      ;
      foreach($tags as $tag) {
          $articleTag = new ArticleTag();
          $articleTag->article_id = $this->id;
          $articleTag->tag_id = (int)$tag;
          $articleTag->save();
      }

  }

  public function getDate()
  {
      return Yii::$app->formatter->asDate($this->date);
  }

  public function getComments()
  {
      return $this->hasMany(Comment::className(),['article_id'=>'id']);
  }
}
